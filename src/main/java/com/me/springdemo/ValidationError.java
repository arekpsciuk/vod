package com.me.springdemo;

import com.me.springdemo.vod.Const;

public class ValidationError extends RuntimeException {
    public ValidationError() {
        super("Validation Error:\n " +
                "Name should be between " +
                Const.MIN_NAME_CHARACTERS +
                " and " +
                Const.MAX_NAME_CHARACTERS +
                " characters \n" +
                "Description cannot be empty \n" +
                "Season number and number of episodes cannot be negative \n" +
                "Year of release must be higher than " + Const.MIN_YEAR);
    }
}
