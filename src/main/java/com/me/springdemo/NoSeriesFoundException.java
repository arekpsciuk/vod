package com.me.springdemo;

public class NoSeriesFoundException extends RuntimeException {
    public NoSeriesFoundException(int id) {
        super("No show with id:" + id + " found!");
    }
}
