package com.me.springdemo;

public class ElementAlreadyExistException extends Exception {
    public ElementAlreadyExistException(int id) {
        super("Element with id: " + id + "already exist");
    }
}
