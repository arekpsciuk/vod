package com.me.springdemo.vod;


import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/vod")
@RestController
public class VodController {

    final VodService service;

    @Autowired
    public VodController(VodService service) {
        this.service = service;
    }

    @PostMapping("/add")
    public Series addSeries(@Valid @RequestBody Series series) {
        return service.addSeries(series);
    }

    @DeleteMapping("/{id}")
    public Series removeSeries(@PathVariable int id) {
        return service.removeSeries(id);
    }

    @PutMapping("/{id}")
    public Series updateSeries(@PathVariable int id, @Valid @RequestBody Series series){
        return service.updateSeries(id,series);
    }

    @GetMapping("/{id}")
    public Series getSeriesById(@PathVariable int id){
        return service.getById(id);
    }

    @GetMapping
    public List<Series> getSeries(@RequestParam(required = false) String name,
                               @RequestParam(required = false) Integer seasonNumber,
                               @RequestParam(required = false) Integer numberOfEpisodes,
                               @RequestParam(required = false) Integer yearOfRelease){
        return service.filter(name,seasonNumber,numberOfEpisodes,yearOfRelease);
    }
}
