package com.me.springdemo.vod;

import com.me.springdemo.NoSeriesFoundException;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component("file")
public class FileVodRepository implements VodRepository {
    private final Path path = Paths.get("src/main/resources/file.txt");

    public FileVodRepository() {
        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Series> get() {
        try {
            BufferedReader bufferedReader = Files.newBufferedReader(path);
            return bufferedReader.lines()
                    .map((String line) -> line.split(","))
                    .map((String[] seriesArr) -> {
                        int id = Integer.parseInt(seriesArr[0]);
                        String name = seriesArr[1];
                        String description = seriesArr[2];
                        int seriesNumber = Integer.parseInt(seriesArr[3]);
                        int yearOfRelease = Integer.parseInt(seriesArr[4]);
                        int numberOfEpisodes = Integer.parseInt(seriesArr[5]);
                        return new Series(id, name, description, seriesNumber, yearOfRelease, numberOfEpisodes);
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public boolean add(Series series) {
        try {
            BufferedWriter writer = Files.newBufferedWriter(path, StandardOpenOption.CREATE,
                    StandardOpenOption.WRITE, StandardOpenOption.APPEND);
            writer.write(series.getId() + "," + series.getName() + "," + series.getDescription() + "," + series.getNumber() + "," +
                    series.getYearOfRelease() + "," + series.getNumberofEpisodes() + "\n");
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean remove(Series series) {
        List<Series> seriesList = get();
        boolean result = seriesList.remove(series);
        if(result) {
            overwrite(seriesList);
        }
        return result;
    }

    @Override
    public Series update(Series oldSeries, Series newSeries) {
        List<Series> seriesList = get();
        int index = seriesList.indexOf(oldSeries);
        if(index>=0){
            Series result = seriesList.set(index,newSeries);
            overwrite(seriesList);
            return result;
        }else {
            throw new NoSeriesFoundException(oldSeries.getId());
        }
    }

    private void overwrite(List<Series>seriesList){
        try {
            Files.deleteIfExists(path);
            Files.createFile(path);
            seriesList.stream()
                    .forEach(this::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
