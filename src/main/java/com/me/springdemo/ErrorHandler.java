package com.me.springdemo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServlet;
import java.util.NoSuchElementException;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Object> noSuchElementException (NoSuchElementException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.I_AM_A_TEAPOT);
    }

    @ExceptionHandler(NoRecipeFound.class)
    public ResponseEntity<Object> noRecipeFound (NoRecipeFound e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.I_AM_A_TEAPOT);
    }

    @ExceptionHandler(NoSeriesFoundException.class)
    public ResponseEntity<Object> noSeriesFoundException (NoSeriesFoundException e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.I_AM_A_TEAPOT);
    }

    @ExceptionHandler(ValidationError.class)
    public ResponseEntity<Object> validationError (ValidationError e){
        return new ResponseEntity<>(e.getMessage(),HttpStatus.I_AM_A_TEAPOT);
    }
}
