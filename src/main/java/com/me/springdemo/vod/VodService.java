package com.me.springdemo.vod;

import com.me.springdemo.NoSeriesFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VodService {
    private final VodRepository vodRepository;
    private int lastId = 0;

    @Autowired
    public VodService(@Qualifier("file") VodRepository vodRepository) {
        this.vodRepository = vodRepository;
    }

    public Series addSeries(String name, String desc, int seasonNumber, int yearOfRelease, int numberOfEpisodes) {
        Series series = new Series(++lastId, name, desc, seasonNumber, yearOfRelease, numberOfEpisodes);
        vodRepository.add(series);
        return series;
    }

    public Series addSeries(Series series){
        series.setId(lastId++);
        vodRepository.add(series);
        return series;
    }

    public Series removeSeries(int id) {
        Series series = getById(id);
        vodRepository.remove(series);
        return series;
    }

    public Series updateSeries(int id, Series series) {
        Series oldSeries = getById(id);
        series.setId(oldSeries.getId());
        vodRepository.update(oldSeries, series);
        return series;
    }

    public Series getById(int id) {
        return vodRepository.get().stream()
                .filter((Series s) -> s.getId() == id)
                .findAny()
                .orElseThrow(() -> new NoSeriesFoundException(id));
    }

    public List<Series> filter(String name, Integer seasonNumber, Integer numberOfEpisodes, Integer yearOfRelease) {
        return vodRepository.get().stream()
                .filter((Series series) -> name == null || series.getName().equals(name))
                .filter((Series series) -> seasonNumber == null || series.getNumber() == seasonNumber)
                .filter((Series series) -> numberOfEpisodes == null || series.getNumberofEpisodes() == numberOfEpisodes)
                .filter((Series series) -> yearOfRelease == null || series.getYearOfRelease() == yearOfRelease)
                .collect(Collectors.toList());
    }
}
