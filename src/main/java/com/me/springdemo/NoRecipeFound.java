package com.me.springdemo;

public class NoRecipeFound extends RuntimeException {
    public NoRecipeFound(int id) {
        super(String.format("No recipe with id %s found", id));
    }
}
