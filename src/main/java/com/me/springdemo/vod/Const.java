package com.me.springdemo.vod;

public class Const {
    public static int MIN_NAME_CHARACTERS = 4;
    public static int MAX_NAME_CHARACTERS = 30;
    public static int MIN_YEAR = 1900;
}
