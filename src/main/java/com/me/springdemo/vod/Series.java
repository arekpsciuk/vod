package com.me.springdemo.vod;

import javax.validation.constraints.*;
import java.util.Objects;

@SuppressWarnings("unused")
public class Series {
    private int id;
    @NotNull
    @Size(min=4,max=30)
    private String name;
    @NotNull
    @NotEmpty
    private String description;
    @PositiveOrZero
    private int number;
    @Min(1900)
    private int yearOfRelease;
    @PositiveOrZero
    private int numberofEpisodes;

    public Series() {
    }

    public Series(int id, String name, String description, int number, int yearOfRelease, int numberofEpisodes) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.number = number;
        this.yearOfRelease = yearOfRelease;
        this.numberofEpisodes = numberofEpisodes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public int getNumberofEpisodes() {
        return numberofEpisodes;
    }

    public void setNumberofEpisodes(int numberofEpisodes) {
        this.numberofEpisodes = numberofEpisodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Series series = (Series) o;
        return id == series.id &&
                number == series.number &&
                yearOfRelease == series.yearOfRelease &&
                numberofEpisodes == series.numberofEpisodes &&
                Objects.equals(name, series.name) &&
                Objects.equals(description, series.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, number, yearOfRelease, numberofEpisodes);
    }
}
