package com.me.springdemo.vod;

import com.me.springdemo.NoSeriesFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Component("list")
public class ListVodRepository implements VodRepository{

    List<Series> seriesList = new ArrayList<>();

    @Override
    public List<Series> get() {
        return seriesList;
    }

    @Override
    public boolean add(Series series) {
        return seriesList.add(series);
    }

    @Override
    public boolean remove(Series series) {
        return seriesList.remove(series);
    }

    @Override
    public Series update(Series oldSeries, Series newSeries) {
        int index = seriesList.indexOf(oldSeries);
        if(index>=0){
            return seriesList.set(index,newSeries);
        }else {
            throw new NoSeriesFoundException(oldSeries.getId());
        }
    }
}
