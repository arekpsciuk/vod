package com.me.springdemo.vod;

import java.util.List;

public interface VodRepository {
    List<Series> get();
    boolean add(Series series);
    boolean remove (Series series);
    Series update (Series oldSeries, Series newSeries);
}
